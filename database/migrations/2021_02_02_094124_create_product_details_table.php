<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->id();
            
            $table->string('product_id');
            $table->string('product_name');
            $table->string('regular_price');
            $table->string('offer_price');
            $table->string('brand');
            $table->string('product_code');
            $table->string('stock');
            $table->string('stock_status');
            $table->string('size');
            $table->string('description');
            $table->string('image');
            $table->string('cover_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
